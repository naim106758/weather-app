import 'dart:convert';
import 'package:http/http.dart';
import 'package:weather/myWaether.dart';
import 'package:weather/repository/weatherRepository.dart';
class WeatherController {
  final WeatherRepository weatherRepository = WeatherRepository();

  Future<MyWeather> getCurrentWeatherController({double?lat,double?lon}) async {
    try {
      Response response = await weatherRepository.getWeatherRepository(lat: lat,lon: lon);
      print(response);
      if (response.statusCode == 200) {
        return MyWeather.fromJson(jsonDecode(response.body));
      } else {
          return MyWeather();
        }
    } catch (error) {
      print(error.toString());
    }
    return MyWeather();
  }
  Future<MyWeather> getCurrentWeatherControllerByCity({String?cityName}) async {
    try {
      Response response = await weatherRepository.getWeatherRepositoryByCity(cityName:cityName);
      print(response);
      if (response.statusCode == 200) {
        return MyWeather.fromJson(jsonDecode(response.body));
      } else {
          return MyWeather();
        }
    } catch (error) {
      print(error.toString());
    }
    return MyWeather();
  }
}

