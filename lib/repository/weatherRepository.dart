// class WeatherRepository {
//   Duration duration = Duration(seconds: 60);
//
//   Future<Response> getWeatherRepository() async {
//     Response response = await Dio()
//         .get(
//             "https://api.openweathermap.org/data/2.5/weather?APPID=a70b2b4432e08da12d9c60a20fef8a23&lon=90.4074&lat=23.7104")
//         .timeout(duration);
//     return response;
//   }
// }
import 'package:http/http.dart';
import 'package:weather/constant/constant.dart';

class WeatherRepository {
  Duration timeOutduration = Duration(seconds: 60);

  Future<Response> getWeatherRepository({double? lat, double? lon}) async {
    Response response = await Client()
        .get(Uri.parse(
        "${ApiConstant.baseUrl}?lat=$lat&units=${ApiConstant.units}&lon=$lon&APPID=${ApiConstant.appId}"))
        .timeout(timeOutduration);
    print(response);
    return response;

  }
  Future<Response> getWeatherRepositoryByCity({String?cityName}) async {
    Response response = await Client()
        .get(Uri.parse(
        "${ApiConstant.baseUrl}?q=$cityName&units=${ApiConstant.units}&APPID=${ApiConstant.appId}"))
        .timeout(timeOutduration);
    return response;
  }
}
