class ApiConstant{
  static const String baseUrl="https://api.openweathermap.org/data/2.5/weather";
  static const String appId="a70b2b4432e08da12d9c60a20fef8a23";
  static const String units="metric";
}