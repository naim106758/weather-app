import 'package:flutter/material.dart';
import 'package:weather/myLocation.dart';
import 'package:weather/myWaether.dart';
import 'package:weather/repository/weatherController.dart';

import 'location_page.dart';

// class LoadingPage extends StatefulWidget {
//   const LoadingPage({
//     Key? key, required myWaether weather,
//   }) : super(key: key);
//
//   @override
//   State<LoadingPage> createState() => _LoadingPageState();
// }
//
// class _LoadingPageState extends State<LoadingPage> {
//   String? mWeather;
//   late bool isLoading;
//   double? mlat;
//   double? mlon;
//
//   @override
//   void initState() {
//     // TODO: implement initState
//     super.initState();
//     getCurrentlocation();
//     isLoading = true;
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       body: SafeArea(
//         child: Container(
//           child: Center(
//             child: isLoading == false
//                 ? Center(child: Text("$mWeather"))
//                 : CircularProgressIndicator(),
//           ),
//         ),
//       ),
//     );
//   }
//
//   void getCurrentlocation() async {
//     MyLocation location = MyLocation();
//     await location.getLocation();
//     fatchCurrentWether(location.lon!, location.lat!);
//   }
//
//   void fatchCurrentWether(double lat, double lon) async {
//     myWaether weatherData =
//         await WeatherController().getCurrentWeatherController(lat: lat, lon: lon);
//     setState(() {
//       isLoading = false;
//     });
//     Navigator.push(context, MaterialPageRoute(builder: (context) {
//       return LoadingPage(weather: weatherData);
//     }));
//   }
// }
class LoadingPage extends StatefulWidget {
  const LoadingPage({Key? key}) : super(key: key);

  @override
  State<LoadingPage> createState() => _LoadingPageState();
}

String? mWeather;
bool? isLoading;

class _LoadingPageState extends State<LoadingPage> {
  double? mLat;
  double? mLon;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    isLoading = true;
    getCurrentLocation();
  }



  void fetchCurrentWeather(double lat, double lon) async {
    MyWeather WeatherData =
    await WeatherController().getCurrentWeatherController(lat: lat, lon: lon);
    setState(() {

      isLoading = false;
    });
    Navigator.push(context, MaterialPageRoute(builder: (context) {
      return LocationPage(
        weather: WeatherData,
      );
    }));
  }

  void getCurrentLocation() async {
    MyLocation location = MyLocation();
    await location.getLocation();

    fetchCurrentWeather(location.lat!, location.lon!);
  }

  /* String margin="abc";
  double? mMargin;*/
  @override
  Widget build(BuildContext context) {
    /* try{mMargin=double.parse(margin);}
    catch(error){
      print(error.toString());
      mMargin=10.0;
    }*/
    return SafeArea(
      child: Scaffold(
        body: Container(
          child: Center(
              child: isLoading == false
                  ? Text(
                "",
                style: const TextStyle(fontSize: 60),
              )
                  : CircularProgressIndicator()),
        ),
      ),
    );
  }
}





