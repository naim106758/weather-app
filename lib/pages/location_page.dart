import 'package:flutter/material.dart';
import 'package:weather/myWaether.dart';
import 'package:weather/pages/city_page.dart';

import '../myLocation.dart';
import '../repository/weatherController.dart';
import 'loading_page.dart';

// class LocationPage extends StatefulWidget {
//   final MyWeather? waether;
//
//   const LocationPage({Key? key, this.waether}) : super(key: key);
//
//   @override
//   State<LocationPage> createState() => _LocationPageState();
// }
//
// class _LocationPageState extends State<LocationPage> {
//   MyWeather? _mywaether;
//   bool? isLoading;
//   int? code;
//
//   @override
//   void initState() {
//     // TODO: implement initState
//     super.initState();
//     _mywaether = widget.waether;
//     isLoading = false;
//     code = _mywaether!.cod;
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//         body: isLoading == false
//             ? code == 200
//                 ? Container(
//                     child: SafeArea(
//                       child: Column(
//                         mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                         children: [
//                           Row(
//                             mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                             children: [
//                               TextButton(
//                                   onPressed: () {
//                                     setState(() {
//                                       isLoading = true;
//                                     });
//                                     getCurrentlocation();
//                                   },
//                                   child: Icon(Icons.near_me)),
//                               TextButton(
//                                   onPressed: () async {
//                                     String cityName = await Navigator.push(
//                                         context,
//                                         MaterialPageRoute(
//                                             builder: (context) => CityName()));
//                                     if (cityName != null) {
//                                       setState(() {
//                                         isLoading = true;
//                                       });
//                                       fatchCurrentWetherByCity(
//                                           cityName: cityName);
//                                     }
//                                   },
//                                   child: Icon(Icons.location_city)),
//                             ],
//                           ),
//                           Padding(
//                             padding: const EdgeInsets.only(left: 15.0),
//                             child: Row(
//                               mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//                               children: [
//                                 Text(
//                                   _mywaether!.main!.temp.toString(),
//                                   style: TextStyle(
//                                       fontSize: 30,
//                                       fontWeight: FontWeight.normal,
//                                       color: Colors.amber),
//                                 ),
//                                 Image.network(
//                                   "http://openweathermap.org/img/wn/${_mywaether!.weather!.first.icon}.png",
//                                   color: Colors.white,
//                                 )
//                               ],
//                             ),
//                           ),
//                           Row(
//                             mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                             children: [
//                               Text(
//                                 'Description',
//                                 style: TextStyle(
//                                     fontSize: 30,
//                                     fontWeight: FontWeight.normal,
//                                     color: Colors.amber),
//                               ),
//                               Text(
//                                 _mywaether!.weather!.first.description
//                                     .toString(),
//                                 style: TextStyle(
//                                     fontSize: 30,
//                                     fontWeight: FontWeight.normal,
//                                     color: Colors.amber),
//                               ),
//                             ],
//                           ),
//                           Row(
//                             mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                             children: [
//                               Text(
//                                 'City Name',
//                                 style: TextStyle(
//                                     fontSize: 30,
//                                     fontWeight: FontWeight.normal,
//                                     color: Colors.amber),
//                               ),
//                               Text(
//                                 _mywaether!.name.toString(),
//                                 style: TextStyle(
//                                     fontSize: 30,
//                                     fontWeight: FontWeight.normal,
//                                     color: Colors.amber),
//                               ),
//                             ],
//                           ),
//                           SizedBox(
//                             height: 4,
//                           )
//                         ],
//                       ),
//                     ),
//                   )
//                 : Center(
//                     child: Text("Error"),
//                   )
//             : Center(child: CircularProgressIndicator()));
//   }
//
//   void getCurrentlocation() async {
//     MyLocation location = MyLocation();
//     await location.getLocation();
//     fatchCurrentWether(location.lon!, location.lat!);
//   }
//
//   void fatchCurrentWether(double lat, double lon) async {
//     MyWeather weatherData = await WeatherController()
//         .getCurrentWeatherController(lat: lat, lon: lon);
//     setState(() {
//       isLoading = false;
//       code = _mywaether!.cod;
//       _mywaether = weatherData;
//     });
//     Navigator.push(context, MaterialPageRoute(builder: (context) {
//       return LoadingPage(weather: weatherData);
//     }));
//   }
//
//   void fatchCurrentWetherByCity({String? cityName}) async {
//     myWaether weatherData = await WeatherController()
//         .getCurrentWeatherControllerByCity(cityName: cityName);
//     setState(() {
//       isLoading = false;
//       code = _mywaether!.cod;
//       _mywaether = weatherData;
//     });
//     Navigator.push(context, MaterialPageRoute(builder: (context) {
//       return LoadingPage(weather: weatherData);
//     }));
//   }
// }
class LocationPage extends StatefulWidget {
  final MyWeather? weather;

  LocationPage({Key? key, this.weather}) : super(key: key);

  @override
  State<LocationPage> createState() {
    return _LocationPageState();
  }
}

class _LocationPageState extends State<LocationPage> {
  MyWeather? _myWeather;
  bool? isLoading;
  int? code;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    isLoading = false;
    _myWeather = widget.weather;

    code= _myWeather!.cod;
  }

  void fetchCurrentWeather(double lat, double lon) async {
    MyWeather weatherData =
    await WeatherController().getCurrentWeatherController(lat: lat, lon: lon);
    setState(() {
      _myWeather = weatherData;
      code= _myWeather!.cod;
      isLoading = false;
    });
    Navigator.push(context, MaterialPageRoute(builder: (context) {
      return LocationPage(
        weather: weatherData,
      );
    }));
  }

  void fetchCurrentWeatherByCity({String? cityName}) async {
    MyWeather weatherData =
    await WeatherController() .getCurrentWeatherControllerByCity(cityName:cityName);
    setState(() {
      _myWeather = weatherData;
      code= _myWeather!.cod;
      isLoading = false;
    });
    Navigator.push(context, MaterialPageRoute(builder: (context) {
      return LocationPage(
        weather: weatherData,
      );
    }));
  }

  void getCurrentLocation() async {
    MyLocation location = MyLocation();
    await location.getLocation();

    fetchCurrentWeather(location.lat!, location.lon!);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: isLoading == false
          ? code==200?Container(
        decoration: BoxDecoration(
            image: DecorationImage(
                image:
                AssetImage("assets/images/location_background.jpg"),
                fit: BoxFit.cover)),
        child: SafeArea(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  TextButton(
                    onPressed: () {
                      setState(() {
                        isLoading = true;
                      });
                      getCurrentLocation();
                    },
                    child: Icon(
                      Icons.near_me,
                      size: 50,
                      color: Colors.white,
                    ),
                  ),
                  TextButton(
                      onPressed: () async {
                        String cityName= await Navigator.push(context,
                            MaterialPageRoute(builder: (context) {
                              return CityPage();
                            }));

                        if(cityName!=null){
                          setState(() {
                            isLoading=true;
                          });
                          fetchCurrentWeatherByCity(cityName:cityName);
                        }
                      },
                      child: Icon(
                        Icons.location_city,
                        size: 50,
                        color: Colors.white,
                      ))
                ],
              ),
              Padding(
                padding: const EdgeInsets.only(left: 20.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Text(
                      _myWeather!.main!.temp.toString(),
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 90,
                          fontWeight: FontWeight.bold),
                    ),
                    Image.network(
                      "http://openweathermap.org/img/wn/${_myWeather!.weather!.first.icon}.png" ,color: Colors.white,),
                  ],
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    "Description:",
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 25,
                        fontWeight: FontWeight.normal),
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Text(
                    _myWeather!.weather!.first.description.toString(),
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 25,
                        fontWeight: FontWeight.bold),
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    "CityName:",
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 25,
                        fontWeight: FontWeight.normal),
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Text(
                    _myWeather!.name.toString(),
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 25,
                        fontWeight: FontWeight.bold),
                  ),
                ],
              ),
              SizedBox(
                height: 5,
              )
            ],
          ),
        ),
      ):Center(child: Text("Error"),)
          : Center(child: CircularProgressIndicator()),
    );
  }
}

